#ifndef __GRAPH_H
#define __GRAPH_H

typedef struct edge {
    int to;
    int weight;
    int in_span_tree;
} Edge;

typedef struct node {
    int socket;
    short own_address;
    int num_edges;
    Edge** edges;
    char marked;
} Node;

int num_nodes;
Node *nodes;

#endif
