#ifndef __TCP_PROTOCOL_H
#define __TCP_PROTOCOL_H

#include <string.h>
#include <sys/types.h>

#include "util.h"

// Defines for Node to Routing-server communication
#define MSG_HEADER_SIZE sizeof(int) + sizeof(int)
#define MSG_DATA_SIZE(num_edges) sizeof(int) * 2 * num_edges

// Defines for Routing-server to Node communication
#define ROUTING_TABLE_HEADER_SIZE sizeof(int)
#define ROUTING_TABLE_DATA_SIZE(num_entries) sizeof(int) * 2 * num_entries

/* 
 * This protocol defines how the nodes and the routing server will communicate with eachother.
 * 
 * The structs msg_edge and node_msg_header are used when the nodes send their info to the routing server,
 * while the structs routing_table_header and routing_table_pair are used when the routing server sends
 * the routing tabels to the nodes.
 *
 * The functions defined in tcp_protocol.c are used to convert from a packet buffer to the different structs
 * and from structs into a packet buffer.
 */

struct msg_edge {
    int to;
    int weight;
};

struct node_msg_header {
    int node_own_address;
    int num_edges;
};

struct routing_table_header {
    int num_entries;
};

struct routing_table_pair {
    int destination;
    int next_hop;
};

void construct_msg_header(char *buff, struct node_msg_header *msg);
void deconstruct_msg_header(char *buff, struct node_msg_header *msg); 

void construct_msg_data(char *buff, struct msg_edge *edge, int num_edges);
void deconstruct_msg_data(char *buff, struct msg_edge *edge, int num_edges);

void construct_routing_table_header(char *buff, struct routing_table_header *header);
void deconstruct_routing_table_header(char *buff, struct routing_table_header *header); 

void construct_routing_table(char *buff, struct routing_table_pair *pair, int num_entries);
void deconstruct_routing_table(char *buff, struct routing_table_pair *pair, int num_entries);

#endif
