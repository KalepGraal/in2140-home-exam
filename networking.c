#include "networking.h"

/* Networking functions */

/* Function to call send() until all bytes have been sent (or failure occurs) 
 * Used when sending over TCP when routing server is communicating with nodes */
ssize_t send_n(int sockfd, const void *buf, size_t len, int flags) {
    ssize_t bytes_sent = 0, total_sent = 0;

    while (((size_t) total_sent) < len) { 
        bytes_sent = send(sockfd, buf+total_sent, len-total_sent, flags);

        if (bytes_sent == -1) {
            LOG(LOGGER_ERROR, "Error at send_n");
            perror("send");
            return -1;
        }
        else if (bytes_sent == 0) {
            break;
        }
        LOG(LOGGER_DEBUG, "Sent %zd/%zu bytes", bytes_sent, len);
        total_sent += bytes_sent;
    }
    LOG(LOGGER_DEBUG, "Sending complete");

    return total_sent;
}

/* Function to call recv() until all bytes have been received (or failure occurs) 
 * Used when sendinh over TCP when routing server is communicating with nodes */
ssize_t recv_n(int sockfd, void *buf, size_t len, int flags) {
    ssize_t bytes_recv = 0, total_recv = 0;

    while (((size_t)total_recv) < len) { 
        bytes_recv = recv(sockfd, buf+total_recv, len-total_recv, flags);
        if (bytes_recv == -1) {
            LOG(LOGGER_ERROR, "Error at recv_n");
            perror("recv");
            return -1;
        }
        if (bytes_recv == 0) {
            break;
        }
        LOG(LOGGER_DEBUG, "Received %zd/%zu bytes", bytes_recv + total_recv, len);
        total_recv += bytes_recv;
    }
    LOG(LOGGER_DEBUG, "Receiving complete");

    return total_recv;
}

// Function to easily configure a sockets settings.
// Used when configuring a TCP or UDP socket.
void set_socket_settings(struct sockaddr_in *sockaddr, unsigned short port, in_addr_t address) {
    sockaddr->sin_family      = AF_INET;  // Assumes we're using IPv4
    sockaddr->sin_port        = htons(port);
    sockaddr->sin_addr.s_addr = address;
}
