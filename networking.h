#ifndef __NETWORKING_H
#define __NETWORKING_H

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <stdlib.h>

#include "util.h"

ssize_t send_n(int sockfd, const void *buf, size_t len, int flags);
ssize_t recv_n(int sockfd, void *buf, size_t len, int flags);

void set_socket_settings(struct sockaddr_in *sockaddr, unsigned short port, in_addr_t address);

#endif
