#ifndef __UDP_PROTOCOL_H
#define __UDP_PROTOCOL_H

#include <string.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <arpa/inet.h>

#include "util.h"

/* 
 * This protocol defines how the nodes communicate with eachother.
 * 
 * Before a node sends a packet to its neighbor, it first creates the packet using construct_packet,
 * which creates a packet buffer from a struct packet.
 * 
 * When a node receives a packet, it is interpreted using deconstruct_packet, which creates a struct packet
 * from the packet buffer received.
 */

struct packet {       
    short length;    // 2 bytes, network byte order
    short dest_addr; // 2 bytes, network byte order
    short src_addr;  // 2 bytes, network byte order
    char *message;   // null-terminated string
};

void construct_packet(unsigned char *buff, struct packet *pkt);

void deconstruct_packet(unsigned char *buff, struct packet *pkt);

#endif
