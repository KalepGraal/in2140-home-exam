#ifndef __GRAPH_ALGORITHMS_H
#define __GRAPH_ALGORITHMS_H

#include <stdlib.h>

#include "print_lib/print_lib.h"
#include "util.h"
#include "graph.h"

#define INFTY 99999
#define MARKED 1
#define NOT_MARKED 0

void create_routing_tables(); 
void dijkstra(int v); 
void dfs_init(int node_own_address);

int *dist;
int **routing_table;

#endif
