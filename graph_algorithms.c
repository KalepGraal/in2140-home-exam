#include "graph_algorithms.h"

/* Creates empty routing_table slots in memory, all values defaults to -1 */
void malloc_routing_tables() {
    int i, j;
    routing_table = malloc(((unsigned int) num_nodes) * sizeof(int*));
    for (i = 0; i < num_nodes; i++)
        routing_table[i] = malloc(((unsigned int) num_nodes) * sizeof(int));

    for (i = 0; i < num_nodes; i++) {
        for (j = 0; j < num_nodes; j++) {
            routing_table[i][j] = -1;
        }
    }
}

/* Creates routing tables for every node */
void create_routing_tables(int node_index) {
    dijkstra(node_index);
    malloc_routing_tables();
    dfs_init(node_index);
}

/* Prints a routing table for a given node */
void print_routing_table(int nodeIndex) {
    int i;
    printf("%d: ", nodes[nodeIndex].own_address);
    for (i = 0; i < num_nodes; i++) {
        if (routing_table[nodeIndex][i] == -1)
            printf("\t%d->X ", nodes[i].own_address);
        else 
            printf("\t%d->%d ", nodes[i].own_address, nodes[routing_table[nodeIndex][i]].own_address);
    } printf("\n");
}

/* Prints all routing tables */
void print_routing_tables() {
    int i;
    printf("ROUTING TABLES:\n");
    for (i = 0; i < num_nodes; i++) {
        print_routing_table(i);
    }
}

/* Sets every nodes marked to NOT_MARKED */
void unmark_all() {
    int i;
    for (i = 0; i < num_nodes; i++) {
        nodes[i].marked = NOT_MARKED;
    }
}

/* Performs a depth first search on a previously created spanning tree. 
 * Checks which nodes can be reached from a node n
 * Adds these nodes to node n's routing table 
 * Since the edges are a part of the minimum spanning tree, 
 * they are guaranteed to be the on optimal path from node 1 */
void dfs(int n) {
    int i, j, iNode;
    nodes[n].marked = MARKED;
    // Node n can send to itself
    routing_table[n][n] = n;

    for (i = 0; i < nodes[n].num_edges; i++) {  
        if (nodes[n].edges[i]->in_span_tree) { 
            iNode = nodes[n].edges[i]->to;  
            if (nodes[iNode].marked == NOT_MARKED) {
                dfs(iNode);
                for (j = 0; j < num_nodes; j++) {
                    if (routing_table[iNode][j] > -1) {
                        LOG(LOGGER_DEBUG, "at [%d] found path to %d via %d", nodes[n].own_address, 
                                nodes[j].own_address, nodes[iNode].own_address);
                        // n can reach j via iNode
                        routing_table[n][j] = iNode;
                    }
                }
            } 
        }
    }
}

/* Initiates a depth first search to create the routing tables */
void dfs_init(int node_index) {
    int i, j, pathlen;
    unmark_all();
    dfs(node_index);

    print_routing_tables();
    printf("\n");

    for (i = 0; i < num_nodes; i++) {
        for (j = 0; j < num_nodes; j++) {
            // Sets pathlen to -1 if not possible to reach j from i, else sets it dist the distance to i
            pathlen = routing_table[i][j] == -1 ? -1 : dist[i];
            print_weighted_edge(nodes[i].own_address, nodes[j].own_address, pathlen);
        }
    }
}

/* Returns the index of the next node with the lowest cost to get to. 
 * Assumes the graph is connected, quits program if this is not the case. 
 * (I assume we won't get a graph with nodes node 1 can't send to).
 * This could probably be replaced with a minHeap, but i won't spend time on implementing one, 
 * as i don't think performance (at this level) is part of the assignment. */
int get_min_node() {
    int i, c_lowest, c_lowest_index = -1;
    c_lowest = INFTY;
    for (i = 0; i < num_nodes; i++) {
        if (dist[i] < c_lowest && nodes[i].marked == NOT_MARKED) {
            c_lowest = dist[i];
            c_lowest_index = i;
        }
    }
    if (c_lowest_index == -1) {
        LOG(LOGGER_ERROR, "Graph is not connected! Aborting!");
        exit(EXIT_FAILURE);
    }
    LOG(LOGGER_DEBUG, "Lowest index = %d", c_lowest_index);
    LOG(LOGGER_DEBUG, "Lowest nodes = %d", nodes[c_lowest_index].own_address);
    return c_lowest_index;
}

/* Creates a minimum spanning tree from Node v. Which is later used to create routing tables */
void dijkstra(int v) {
    int i, j, added_nodes = 0, z_weight;
    int u, z;
    // from keeps track of the currently best path to reach a node 
    int from[num_nodes];
    // dist keeps track of the current shortest distance to a node
    dist = malloc(((short unsigned int) num_nodes) * sizeof(int));

    unmark_all();

    // Setting all nodes to infinite weight
    for(i = 0; i < num_nodes; i++) 
        dist[i] = INFTY;

    // Initialize source node v
    dist[v] = 0;
    nodes[v].marked = MARKED;  
    from[v] = v;
    added_nodes++;

    // Setting weight to those adjacent to source node v
    for (i = 0; i < nodes[v].num_edges; i++) {
        dist[nodes[v].edges[i]->to] = nodes[v].edges[i]->weight;
        from[nodes[v].edges[i]->to] = v;
    }

    // Initialize from[], for the first node we're checking
    from[get_min_node()] = v;

    // Run until all nodes are marked
    while (added_nodes < num_nodes) {
        u = get_min_node();
        nodes[u].marked = MARKED;
        added_nodes++;

        // for each node z adjacent to u such that z is not marked
        for (i = 0; i < nodes[u].num_edges; i++) {
            z = nodes[u].edges[i]->to;
            z_weight = nodes[u].edges[i]->weight;
            if(nodes[z].marked == NOT_MARKED) {
                // perform the relaxation procedure on edge (u, z)
                if ((dist[u] + z_weight) < dist[z]) {
                    dist[z] = dist[u] + z_weight;
                    from[z] = u;
                }
            }
        }
    }

    // Create spanning tree by adding all edges that leads to a nodes best path
    for (u = 0; u < num_nodes; u++) {

        v = from[u];
        
        // add edge from u to v to spanning tree
        for (j = 0; j < nodes[u].num_edges; j++) {
            if (nodes[u].edges[j]->to == v) {
                nodes[u].edges[j]->in_span_tree = 1;
            }
        }
        // add edge from v to u to spanning tree
        for (j = 0; j < nodes[v].num_edges; j++) {
            if (nodes[v].edges[j]->to == u) {
                nodes[v].edges[j]->in_span_tree = 1;
            }
        }
    }
}
