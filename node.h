#ifndef __NODE_H
#define __NODE_H

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "networking.h"
#include "print_lib/print_lib.h"
#include "util.h"
#include "tcp_protocol.h"
#include "udp_protocol.h"

#define NODE_USAGE "Usage: node <Port> <OwnAddress> <NeighbourAddress>:<weight> ...\n<Port> Value between 1024 and 65535-number of nodes\n<OwnAddress> Node's address written as decimal number (1-1023)\n<NeighbourAddress>:<weight>\n\t<NeighbourAddress> - Neighbour's address\n\t<weight> - Link's weight to corresponding neighbour (an integer number)\n"

#define ENABLE_SO_REUSEADDR 1
#define PACKET_MAX_SIZE 1024

#endif
