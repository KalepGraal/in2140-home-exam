#include "tcp_protocol.h"

/* Protocol for communication between routing server and nodes */

/* Creates header for packet buffer used when sending node info from node to routing server. */
void construct_msg_header(char *buff, struct node_msg_header *msg) {
    memcpy(&buff[0], &(msg->node_own_address), sizeof(int));
    memcpy(&buff[sizeof(int)], &(msg->num_edges), sizeof(int));
}

/* Retrieves data from the header of the packet buffer used when sending node info from node to routing server */
void deconstruct_msg_header(char *buff, struct node_msg_header *msg) {
    memcpy(&(msg->node_own_address), &buff[0], sizeof(int));
    memcpy(&(msg->num_edges),        &buff[4], sizeof(int));
}

/* Adds data about all of the nodes edges to the packet buffer */
void construct_msg_data(char *buff, struct msg_edge *edges, int num_edges) {
    int i, offset;
    for (i = 0; i < num_edges; i++) {
        offset = sizeof(int)*2 + sizeof(int) * 2 * i;
        memcpy(&buff[offset],               &(edges[i].to), sizeof(int));
        memcpy(&buff[offset + sizeof(int)], &(edges[i].weight), sizeof(int));
    }
}

/* Retrieves data from packet buffer containing all of the nodes edges */
void deconstruct_msg_data(char *buff, struct msg_edge *edges, int num_edges) {
    int i, offset;
    for (i = 0; i < num_edges; i++) {
        offset = sizeof(int) * 2 * i;
        memcpy(&(edges[i].to),     &buff[offset], sizeof(int));
        memcpy(&(edges[i].weight), &buff[offset + sizeof(int)], sizeof(int));
    }
}

/* Creates header for packet used when routing server sends routing table to node */
void construct_routing_table_header(char *buff, struct routing_table_header *header) {
    memcpy(&buff[0], &header->num_entries, sizeof(int));
}

/* Retrieves data from the header of the packet buffer used when routing server sends routing table to node */
void deconstruct_routing_table_header(char *buff, struct routing_table_header *header) {
    memcpy(&header->num_entries, &buff[0], sizeof(int));
}

/* Adds all routing pairs to the packet buffer */
void construct_routing_table(char *buff, struct routing_table_pair *pairs, int num_entries) {
    int i, offset;
    for (i = 0; i < num_entries; i++) {
        offset = sizeof(int) + sizeof(int) * 2 * i;
        memcpy(&buff[offset], &pairs[i].destination, sizeof(int)); 
        memcpy(&buff[offset + sizeof(int)], &pairs[i].next_hop, sizeof(int)); 
    }
}

/* Retrieves all routing pairs from packet buffer */
void deconstruct_routing_table(char *buff, struct routing_table_pair *pairs, int num_entries) {
    int i, offset;
    for (i = 0; i < num_entries; i++) {
        offset = sizeof(int) * 2 * i;
        memcpy(&pairs[i].destination, &buff[offset], sizeof(int)); 
        memcpy(&pairs[i].next_hop, &buff[offset + sizeof(int)], sizeof(int)); 
    }
}
