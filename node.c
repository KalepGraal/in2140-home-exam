#include "node.h"

unsigned short BASE_PORT;
short own_address;
int NODE_PORT;

int num_edges;
struct msg_edge *edges;
int num_entries;
struct routing_table_pair *pairs;

/* Prints all of the nodes edges */
void print_edges() {
    int i;
    LOG(LOGGER_INFO, "This node has %d edges", num_edges);
    for (i = 0; i < num_edges; i++) {
        LOG(LOGGER_INFO, "Edge to: %d, weight: %d", edges[i].to, edges[i].weight);
    }
}

/* Connects to a socket. Runs once everytime you attempt to send to the same node...
 * Should've been changed so that it reuses previous sockets if it has sent to a node previously */
int get_socket(char *address, unsigned short port, int type, struct sockaddr_in *server_addr) {
    int sockfd;

    LOG(LOGGER_DEBUG, "Attempting to create socket");
    sockfd = socket(AF_INET, type, 0);
    if (sockfd == -1) {
        LOG(LOGGER_ERROR, "Failed to create socket");
        perror("socket");
        exit(EXIT_FAILURE);
    }

    set_socket_settings(server_addr, port, inet_addr(address));

    return sockfd;
}

/* Sets up connection to the routing server */
int setup_tcp(char *address, unsigned short port) {
    int sockfd, ret;
    struct sockaddr_in server_addr;

    sockfd = get_socket(address, port, SOCK_STREAM, &server_addr);

    LOG(LOGGER_DEBUG, "Connecting to server at %s:%d", address, port);

    ret = connect(sockfd, (struct sockaddr*) &server_addr, sizeof(struct sockaddr_in));
    if (ret == -1) {
        LOG(LOGGER_ERROR, "Failed to connect to routing server at %s:%d", address, port);
        perror("socket");
        exit(-2);
    }

    return sockfd;
}

/* Creates an unsigned char buffer to store a packet in. Used to easier call upon given print_lib */
unsigned char *create_pkt_buff(struct packet *pkt) {
    unsigned char *buff;
    buff = malloc((size_t)pkt->length);
    construct_packet(buff, pkt);
    return buff;
}

/* Sends a packet towards its destination */
void send_packet(struct packet *pkt) {
    int i, j, socketfd;
    unsigned short next_port;
    struct sockaddr_in sockaddr;

    unsigned char *buff = create_pkt_buff(pkt);

    for (i = 0; i < num_entries; i++) {
        if (pairs[i].destination == pkt->dest_addr) {
            LOG(LOGGER_DEBUG, "Found correct pair");
            for (j = 0; j < num_edges; j++) {
                if (edges[j].to == pairs[i].next_hop) {
                    LOG(LOGGER_INFO, "Sending packet to %d to reach %d", pairs[i].next_hop, pairs[i].destination);

                    next_port = (unsigned short) (BASE_PORT + pairs[i].next_hop);
                    socketfd = get_socket("127.0.0.1", next_port, SOCK_DGRAM, &sockaddr);

                    sendto(socketfd, buff, (size_t)pkt->length, 0, 
                            (struct sockaddr *) &sockaddr, sizeof(struct sockaddr_in));
                    free(buff);
                    return;
                }
            }
        }
    }

    LOG(LOGGER_ERROR, "[%d] Failed to send packet to: %d\nmsg: %s", own_address, pkt->dest_addr, pkt->message);

    free(buff);

}

/* Binds to the nodes udp socket and returns the socketfd */
int bind_to_udp_socket() {
    int ret, udp_socket, port, reuse = ENABLE_SO_REUSEADDR;
    struct sockaddr_in sockaddr;

    port = BASE_PORT + own_address;
    if (port < 1024 || port > 65535) {
        LOG(LOGGER_ERROR, "P+A is an invalid port-number, outside legal range (1024-65535)");
        fprintf(stderr, "Invalid port number\n");
        exit(-1);    
    }

    LOG(LOGGER_DEBUG, "Should try to bind to UDP socket at port: %d", (int)BASE_PORT+own_address);

    // Creates UDP socket
    udp_socket = socket(AF_INET, SOCK_DGRAM, 0);
    if (udp_socket == -1) {
        LOG(LOGGER_ERROR, "failed to create udp socket");
        fprintf(stderr, "failed to create udp socket\n");
        perror("socket");
        exit(-1);
    }

    // Sets port reusable
    setsockopt(udp_socket, SOL_SOCKET, SO_REUSEADDR, &reuse, sizeof(int)); 

    // Configures socket settings
    set_socket_settings(&sockaddr, (unsigned short) port, INADDR_ANY);

    // Binds to socket
    ret = bind(udp_socket, (struct sockaddr*) &sockaddr, sizeof(sockaddr));
    if (ret == -1) {
        LOG(LOGGER_ERROR, "Failed to bind to socket");
        perror("bind");
        fprintf(stderr, "Failed to bind to socket\n");
        exit(EXIT_FAILURE);
    }
    LOG(LOGGER_DEBUG, "bound to udp socket at port %d", port);

    return udp_socket;
}

/* Checks if a packet has reached its destination or if not sends it towards its destination
 * if this is the packets destination, it inspects the packet, to see whether to quit or not
 * will quit (return 0) if the packet contains message "QUIT" */
int handle_packet(struct packet *pkt, unsigned char *buff) {
    if (pkt->dest_addr == own_address) {
        // Packet has reached destination
        print_received_pkt(own_address, buff);

        LOG(LOGGER_INFO, "Packet is to me from %d", pkt->src_addr);
        if (strcmp(pkt->message, "QUIT") == 0) {
            LOG(LOGGER_DEBUG, "PACKET TOLD ME TO QUIT, QUITTING");
            free(pkt->message);
            return 0;
        }
    } else {
        print_forwarded_pkt(own_address, buff);
        LOG(LOGGER_INFO, "Forwarding the packet");
        send_packet(pkt);
    }
    LOG(LOGGER_DEBUG, "Packet handled");
    free(pkt->message);
    return 1;
}

/* Receives incoming packets. Will block until a packet arrives. 
 * Returns 1 if more packets are arriving, 0 if not */
int receive_packet(int sockfd) {
    int ret;
    ssize_t bytes_received;
    size_t size;
    unsigned char *buff;
    struct packet pkt;

    LOG(LOGGER_DEBUG, "Waiting for packet");

    size = PACKET_MAX_SIZE;
    buff = malloc(size);

    // Receive packet buffer
    bytes_received = recvfrom(sockfd, buff, size, 0, NULL, NULL);
    if (bytes_received == -1) {
        LOG(LOGGER_ERROR, "Error at recvfrom");
        perror("recvfrom");
        exit(EXIT_FAILURE);
    }

    // Insert into struct from buffer
    deconstruct_packet(buff, &pkt);

    LOG(LOGGER_INFO, "Received a packet");

    LOG(LOGGER_DEBUG, "PKT: [len=%d, dest=%d, src=%d]", pkt.length, pkt.dest_addr, pkt.src_addr);
    LOG(LOGGER_DEBUG, "PKT: [msg=%s]", pkt.message);

    ret = handle_packet(&pkt, buff);
    free(buff);

    return ret;
}

/* Sends information about the node to the ruting server */
ssize_t send_node_info(int socketfd) {
    size_t msg_size;
    ssize_t bytes_sent;
    char *buff;
    struct node_msg_header header;

    msg_size = MSG_HEADER_SIZE + MSG_DATA_SIZE(num_edges); 
    buff = malloc(msg_size);
    LOG(LOGGER_DEBUG, "malloc'd (%zu) bytes for msg_size", msg_size);

    header.node_own_address = own_address;
    header.num_edges = num_edges;

    // Creates packet buffer to send
    construct_msg_header(buff, &header); 
    construct_msg_data(buff, edges, num_edges);

    bytes_sent = send_n(socketfd, buff, msg_size, 0);
    LOG(LOGGER_DEBUG, "Sent %zd bytes to server!", bytes_sent);

    free(buff);
    return bytes_sent;
}

/* Retrieves routing table from the routing server */
void get_routing_table(int socketfd) {
    ssize_t bytes_received;
    struct routing_table_header header;
    char *buff;

    buff = malloc(ROUTING_TABLE_HEADER_SIZE);

    LOG(LOGGER_INFO, "Requesting routing table");

    LOG(LOGGER_DEBUG, "requesting routing_table_header");
    bytes_received = recv_n(socketfd, buff, ROUTING_TABLE_HEADER_SIZE, 0);
    LOG(LOGGER_DEBUG, "received %zd bytes", bytes_received);

    deconstruct_routing_table_header(buff, &header);
    LOG(LOGGER_DEBUG, "num_entries = %d", header.num_entries);
    num_entries = header.num_entries;

    if (num_entries > 0) {
        // Allocate memory for packet
        LOG(LOGGER_DEBUG, "reallocing %zu bytes for pairs", ROUTING_TABLE_DATA_SIZE(num_entries));
        buff = realloc(buff, ROUTING_TABLE_DATA_SIZE(num_entries));

        // get packet
        LOG(LOGGER_DEBUG, "requesting routing_table_pairs");
        bytes_received = recv_n(socketfd, buff, ROUTING_TABLE_DATA_SIZE(num_entries), 0);
        LOG(LOGGER_DEBUG, "received %zd bytes", bytes_received);

        // extract pairs from packet
        pairs = malloc(ROUTING_TABLE_DATA_SIZE(num_entries));
        deconstruct_routing_table(buff, pairs, num_entries);
    } else {
        LOG(LOGGER_DEBUG, "no pairs sent here, endpoint");
    }
    LOG(LOGGER_INFO, "Got routing table");

    free(buff);

}

/* Prints this nodes routing table */
void print_routing_table() {
    int i;
    for (i = 0; i < num_entries; i++) {
        printf("%d via %d | ", pairs[i].destination, pairs[i].next_hop);
    }
    printf("\n");
}

/* Function ran by the sender node (own_address = 1) 
 * Creates packets from data.txt, and sends them towards their destinations */
void run_sender_node() {
    LOG(LOGGER_INFO, "Reading from file to create and send packets");
    
    FILE *file;
    file = fopen("data.txt", "r");
    if (file == NULL) {
        LOG(LOGGER_ERROR, "Failed to open data.txt");
        perror("fopen");
        exit(EXIT_FAILURE);
    }
    char *line;
    line = malloc(1024);    // Assumes a line wont need more than 1024 bytes 
    while (!feof(file)) {
        struct packet pkt;
        int i = 0;
        char ch = getc(file);
        line[i++] = ch;
        // Stores the entire line in the line buffer
        while(ch != '\n' && ch != EOF) {
            ch = getc(file);
            line[i++] = ch;
        }
        if (ch == EOF) {
            LOG(LOGGER_DEBUG, "EOF reached");
            break;
        }
        line[i] = '\0';

        // Extracts packet destination and message from line buffer
        int dest = atoi(strtok(line, " "));
        char *msg = strtok(NULL, "\n");
        
        // Creates packet
        pkt.length    = 6 + strlen(msg) + 1;
        pkt.dest_addr = dest;
        pkt.src_addr  = own_address;
        pkt.message   = msg;

        LOG(LOGGER_INFO, "Created a packet with destination %d", pkt.dest_addr);

        // Creates a temporary buffer to call print_libs print_pkt()
        unsigned char *buff = create_pkt_buff(&pkt);
        print_pkt(buff);

        // Was unsure whether node 1 should call print_received_pkt, so i included it just in case
        if (pkt.dest_addr == own_address) {
            print_received_pkt(own_address, buff);
        } else {
            // Sends packet towards destination
            // Assumes node 1 is not supposed to call print_forwarded_pkt
            send_packet(&pkt);
        }
        free(buff);
    }

    fclose(file);
    free(line);
}

/* Starts the node. Takes BASE_PORT, own_adress, and edges as parameters */
int main(int argc, char **argv) {
    int udp_socket, i, port, routing_socket, receiving;
    
    if (argc < 3) {
        LOG(LOGGER_ERROR, "not enough arguments to start a node!");
        printf(NODE_USAGE);
        exit(EXIT_FAILURE);
    }

    port = atoi(argv[1]);
    own_address = (short) atoi(argv[2]);

    if (port < 1024 || port > 65535) {
        LOG(LOGGER_ERROR, "Base port %d outside legal range! (1024-65535)", port);
        exit(EXIT_FAILURE);
    }
    BASE_PORT = (unsigned short) port;
    NODE_PORT = BASE_PORT + own_address;

    num_edges = argc - 3;
    
    LOG(LOGGER_DEBUG, "Node num: %d, at port %d",  own_address, BASE_PORT);
    LOG(LOGGER_DEBUG, "num_edges = %d", num_edges);
    
    // Creates edges
    edges = malloc(((unsigned int) num_edges) * sizeof(struct msg_edge));
    for (i = 0; i < num_edges; i++) {
        edges[i].to     = atoi(strtok(argv[i+3], ":"));
        edges[i].weight = atoi(strtok(NULL, ":"));
    }
    print_edges();

    udp_socket = bind_to_udp_socket();

    routing_socket = setup_tcp("127.0.0.1", BASE_PORT);;

    send_node_info(routing_socket);

    get_routing_table(routing_socket);
    close(routing_socket);

    print_routing_table();

    // Assumes here that node 1 won't be receiving data (only sending)
    if (own_address == 1) {
        LOG(LOGGER_DEBUG, "This is the sender node");
        LOG(LOGGER_DEBUG, "Waiting 1 second before sending");
        sleep(1);

        run_sender_node();

        LOG(LOGGER_INFO, "Done sending");
    } else {
        receiving = 1;
        while (receiving) 
            receiving = receive_packet(udp_socket);
        // QUIT has been received, cleanup and exit
    }

    // Cleanup
    close(udp_socket);
    free(edges);
    free(pairs);
    
    printf("Node finished successfully\n");

    return EXIT_SUCCESS;
}

