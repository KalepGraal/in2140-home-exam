#include "udp_protocol.h"

/* Creates a packet buffer from a struct packet. 
 * The short values in the struct are first converted to network byte order
 * before they are insterted into the packet buffer */
void construct_packet(unsigned char *buff, struct packet *pkt) {
    int a; // stores network byte order
    a = htons(pkt->length);
    memcpy(&buff[0], &a,    sizeof(short));
    a = htons(pkt->dest_addr);
    memcpy(&buff[2], &a, sizeof(short));
    a = htons(pkt->src_addr);
    memcpy(&buff[4], &a,  sizeof(short));

    memcpy(&buff[6], pkt->message,   pkt->length-6);
}

/* Creates a packet struct from a buffer.
 * The buffers short values are converted from network byte order to host byte order
 * The struct packets message is allocated as large as needed, and should be freed later */
void deconstruct_packet(unsigned char *buff, struct packet *pkt) {
    memcpy(&(pkt->length),    &buff[0], sizeof(short));
    memcpy(&(pkt->dest_addr), &buff[2], sizeof(short));
    memcpy(&(pkt->src_addr),  &buff[4], sizeof(short));
    
    // Convert from network byte order to host byte order
    pkt->length    = ntohs(pkt->length);
    pkt->dest_addr = ntohs(pkt->dest_addr);
    pkt->src_addr  = ntohs(pkt->src_addr);

    // Allocates memory for the message, should be freed later
    pkt->message = malloc(pkt->length - 6);
    memcpy(pkt->message,   &buff[6], pkt->length-6);
}
