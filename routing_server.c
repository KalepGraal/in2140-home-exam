#include "routing_server.h"

void cleanup();

/* Finds the node with a given own_address */
int find_node(int num) {
    int i;
    for (i = 0; i < num_nodes; i++) {
        if (nodes[i].own_address == num)
            return i;
    }
    LOG(LOGGER_ERROR, "Attempting to find node with invalid own_address: %d", num);
    cleanup();
    exit(EXIT_FAILURE);
}

/* Prints node info along with its edges */
void print_node(Node *node) {
    int i;
    printf("Node own_address %d \t: num_edges[%d] to: ", node->own_address, node->num_edges);
    
    for (i = 0; i < node->num_edges; i++) {
        printf("%d(w=%d), ", nodes[node->edges[i]->to].own_address, node->edges[i]->weight);
    }
    printf("\n");
}

/* Creates an edge between two nodes (the edges point to the neighbours index in nodes[])
 * this requires all nodes to be added first */
Edge *create_edge(int to, int weight) {
    LOG(LOGGER_DEBUG, "Creating edge to: %d", to);
    Edge *edge = malloc(sizeof(Edge));
    edge->to = find_node(to);
    edge->weight = weight;
    edge->in_span_tree = 0;
    return edge;
}

/* Removes an edge, used if validation fails.
 * Will result in empty spaces in memory, but will effectively ignore the edge */
void remove_edge(int node_index, int edge_index) {
    int i;
    for (i = edge_index; i < nodes[node_index].num_edges - 1; i++) {
        nodes[node_index].edges[i] = nodes[node_index].edges[i+1];
    }
    nodes[node_index].num_edges--;
}

/* Checks that every edge is reported twice with the same weight */
void validate_edges() {
    int i, j, k, reportedTwice;
    LOG(LOGGER_INFO, "Validating edges");

    for (i = 0; i < num_nodes; i++) {
        for (j = 0; j < nodes[i].num_edges; j++) {
            Node toNode = nodes[nodes[i].edges[j]->to];
            reportedTwice = 0;
            for (k = 0; k < toNode.num_edges; k++) {
                if (toNode.edges[k]->to == i && toNode.edges[k]->weight == nodes[i].edges[j]->weight)
                    reportedTwice = 1;
            }
            if (!reportedTwice) {
                LOG(LOGGER_INFO, "Edge from %d to %d did not report twice, removing", 
                        nodes[i].own_address, toNode.own_address);
                remove_edge(i,j);
            }
        }
    }
    LOG(LOGGER_INFO, "Validated all edges");
}

/* Exit function that creates a bit more readability, not really needed */
void exit_failure(char *reason, char *failure_point) {
    LOG(LOGGER_ERROR, "%s", reason);
    perror(failure_point);
    exit(EXIT_FAILURE);
}

/* Attempts to create and bind to a TCP socket at a given port */
int create_tcp_socket(unsigned short tcp_port) {
    int tcp_socket, ret, reuse = ENABLE_SO_REUSEADDR;
    struct sockaddr_in sockaddr; 

    LOG(LOGGER_DEBUG, "Attempting to create tcp_socket");
    tcp_socket = socket(AF_INET, SOCK_STREAM, 0);
    if (tcp_socket == -1) exit_failure("Failed to create socket", "socket");
    
    // sets port reusable, as adviced in the assignment
    setsockopt(tcp_socket, SOL_SOCKET, SO_REUSEADDR, &reuse, sizeof(int)); 

    // Configures the sockets settings
    set_socket_settings(&sockaddr, tcp_port, INADDR_ANY);

    LOG(LOGGER_DEBUG, "Attempting to name (bind) the socket");
    ret = bind(tcp_socket, (struct sockaddr*) &sockaddr, sizeof(sockaddr));
    if (ret) exit_failure("Failed to name (bind) the socket", "bind");

    LOG(LOGGER_DEBUG, "Attempting to listen to socket");
    // marks socket as a passive socket, and sets the max number of pending connections to the number of nodes
    ret = listen(tcp_socket, num_nodes); 
    if (ret) exit_failure("listen to socket failed", "listen");
    LOG(LOGGER_DEBUG, "listening");

    return tcp_socket;
}

/* 
 * Sends routing-table pairs to all nodes. 
 *
 * It would probably be easiest to just send the entire routing table here,
 * but interpreting the assignment it seems only those pairs that are used should be sent,
 * so thats what im doing.
 * */
void send_routing_tables() {
    int i, j, through, header_size, data_size, packet_size, entries, num_added, bytes_sent;
    struct routing_table_header header;
    struct routing_table_pair *pairs;
    char *buff;

    for (i = 0; i < num_nodes; i++) {
        // Checks how many pairs to send.
        entries = 0;
        for (j = 0; j < num_nodes; j++) {
            through = routing_table[i][j];
            if (through != -1 && through != i) 
                entries++;
        }
        header.num_entries = entries;

        header_size = ROUTING_TABLE_HEADER_SIZE;
        data_size = ROUTING_TABLE_DATA_SIZE(entries);
        packet_size = header_size + data_size;
       
        buff = malloc(packet_size);
        LOG(LOGGER_DEBUG, "Allocated %d bytes to send table to %d", packet_size, nodes[i].own_address);

        // Creates routing table packet header
        construct_routing_table_header(buff, &header);
        LOG(LOGGER_DEBUG, "made routing table header");

        pairs = malloc(data_size);
        
        // Creates all routing pairs
        num_added = 0;
        for (j = 0; j < num_nodes; j++) {
            through = routing_table[i][j];
            // Checks that pair exists, and is not to itself
            if (through != -1 && through != i) {
                pairs[num_added].destination = nodes[j].own_address;
                pairs[num_added].next_hop    = nodes[through].own_address;
                LOG(LOGGER_DEBUG, "added pair (%d, %d)", pairs[num_added].destination, pairs[num_added].next_hop);

                num_added++;
            }
        }

        // Adds the pairs to the routing table packet
        construct_routing_table(buff, pairs, entries);
        
        LOG(LOGGER_DEBUG, "sending %d entries to %d: ", entries, nodes[i].own_address);
        bytes_sent = send_n(nodes[i].socket, buff, packet_size, 0);
        LOG(LOGGER_DEBUG, "Sent %d bytes to %d", bytes_sent, nodes[i].own_address);

        free(pairs);
        free(buff);

    }
    LOG(LOGGER_INFO, "Sent routing tables to all nodes");
}

/* Adds connecting nodes */
void accept_nodes(int tcp_socket) {
    int i, j, num_accepted;
    struct sockaddr_in node_addr;
    socklen_t addrlen = sizeof(struct sockaddr_in);
    struct node_msg_header msg_header;
    struct msg_edge *edges;

    num_accepted = 0;

    char *msg_buf = malloc(MSG_HEADER_SIZE);
    // Add all nodes
    for (i = 0; i < num_nodes; i++) {
        // waits until a new node connects, then adds info
        nodes[i].socket = accept(tcp_socket, (struct sockaddr*) &node_addr, &addrlen);
        if (nodes[i].socket == -1) exit_failure("Failed to accept node", "accept");
        LOG(LOGGER_DEBUG, "accepted nodes: %d, last at port: %d", num_accepted++, ntohs(node_addr.sin_port));

        // Receives the nodes own_address and num_edges through the packet header
        recv_n(nodes[i].socket, msg_buf, MSG_HEADER_SIZE, 0);
        deconstruct_msg_header(msg_buf, &msg_header);

        nodes[i].own_address = msg_header.node_own_address;
        nodes[i].num_edges   = msg_header.num_edges;

        LOG(LOGGER_DEBUG, "accepted node is node own_address %d, with %d edges", 
                nodes[i].own_address, nodes[i].num_edges);
    }

    // add all edges
    for (i = 0; i < num_nodes; i++) {
        nodes[i].edges = malloc(((unsigned int)nodes[i].num_edges) * sizeof(Edge*));
        
        if (nodes[i].num_edges > 0) {
            int data_size = MSG_DATA_SIZE(nodes[i].num_edges);
            msg_buf = realloc(msg_buf, data_size);

            // Receives the rest of the packet containing nodes[i]s edges
            recv_n(nodes[i].socket, msg_buf, data_size, 0);

            edges = malloc(MSG_DATA_SIZE(nodes[i].num_edges));

            deconstruct_msg_data(msg_buf, edges, nodes[i].num_edges);

            for (j = 0; j < nodes[i].num_edges; j++) {
                nodes[i].edges[j] = create_edge(edges[j].to, edges[j].weight);
            }
            free(edges);
        }
    }
    free(msg_buf);

    // Checks that all edges occured twice
    validate_edges();

    // Prints out the nodes accepted 
    printf("NODES: \n");
    for (i = 0; i < num_nodes; i++) {
        print_node(&nodes[i]);
    }

}

/* Closes the routing servers TCP socket, and all sockets to the nodes */
void close_sockets(int tcp_socket) {
    int i, ret;
    for (i = 0; i < num_nodes; i++) {
        ret = close(nodes[i].socket);
        if (ret == -1) {
            LOG(LOGGER_ERROR, "Could not close socket to node %d", nodes[i].own_address);
            perror("close");
        } else {
            LOG(LOGGER_DEBUG, "Closed socket to node %d", nodes[i].own_address);
        }
    }

    ret = close(tcp_socket);
    if (ret == -1) {
        LOG(LOGGER_ERROR, "Could not close TCP socket");
        perror("close");
    } else {
        LOG(LOGGER_DEBUG, "Successfully closed TCP socket");
    }
}

/* Frees allocated memory */
void cleanup() {
    int i, j;
    LOG(LOGGER_DEBUG, "Freeing allocated memory");
    for (i = 0; i < num_nodes; i++) {
        for (j = 0; j < nodes[i].num_edges; j++) {
            free(nodes[i].edges[j]);
        }
        free(nodes[i].edges);
        free(routing_table[i]);
    }
    free(nodes);
    free(dist);
    free(routing_table);
}

int main(int argc, char **argv) {
    int port, tcp_socket;
    unsigned short tcp_port;

    if (argc < 3) {
        LOG(LOGGER_ERROR, "Not enough arguments to start routing_server!");
        printf(ROUTING_SERVER_USAGE);
        exit(EXIT_FAILURE);
    }
    port      = atoi(argv[1]);
    num_nodes = atoi(argv[2]);   
    nodes = malloc(((unsigned int) num_nodes) * sizeof(Node));

    if (port < 1024 || port > 65535) {
        LOG(LOGGER_ERROR, "TCP_PORT %d outside legal range! (1024-65535)", port);
        exit(EXIT_FAILURE);
    }
    tcp_port  = (unsigned short) port;

    LOG(LOGGER_INFO, "Should listen to port: %d, num_nodes = %d", tcp_port, num_nodes);

    tcp_socket = create_tcp_socket(tcp_port);
    
    accept_nodes(tcp_socket);

    create_routing_tables(find_node(1));

    send_routing_tables();
    close_sockets(tcp_socket);

    cleanup();
    LOG(LOGGER_INFO, "Finished");
    printf("Routing server finished successfully\n");
    return EXIT_SUCCESS;
    
}

