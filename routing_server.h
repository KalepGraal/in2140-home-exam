#ifndef __ROUTING_SERVER_H
#define __ROUTING_SERVER_H

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include "routing_server.h"
#include "print_lib/print_lib.h"
// Benytter meg av util.h fra plenum for enkel debugging
#include "util.h"
#include "networking.h"
#include "tcp_protocol.h"
#include "graph.h"
#include "graph_algorithms.h"

#define ENABLE_SO_REUSEADDR 1
#define ROUTING_SERVER_USAGE "Usage: routing_server <P> <N>\n<P> the TCP port on which the server listens.\n<N> The number of nodes\n"

#endif
