CCFLAGS= -g -Wall -Wextra
PORT = 2000

all: routing_server node

routing_server: routing_server.c util.c print_lib/print_lib.c graph_algorithms.c networking.c tcp_protocol.c
	gcc $(CCFLAGS) $^ -o $@

node: node.c util.c networking.c tcp_protocol.c udp_protocol.c print_lib/print_lib.c
	gcc $(CCFLAGS) $^ -o node

run: clean all
	bash run_1.sh $(PORT)
	bash run_2.sh $(PORT)

run1: clean all
	bash run_1.sh $(PORT)

run2: clean all
	bash run_2.sh $(PORT)

clean:
	rm -f routing_server
	rm -f node
	rm -f logfile.txt
